export function choosePet(Pet) {
  // choosepet is an ActionCreator, it needs to return an action,
  // an object with a type property.
  return {
    type: "PET_SELECTED",
    payload: Pet
  };
}
