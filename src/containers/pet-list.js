import React, { Component } from "react";
import { connect } from "react-redux";
import { choosePet } from "../actions/index";
import { bindActionCreators } from "redux";

class PetList extends Component {
  renderList() {
    return this.props.Pets.map(Pet => {
      return (
        <li
          key={Pet.name}
          onClick={() => this.props.choosePet(Pet)}
          className="list-group-item"
        >
          {Pet.name}
          {Pet.type}
        </li>
      );
    });
  }

  render() {
    return (
      <ul className="list-group col-sm-4">
        {this.renderList()}
      </ul>
    );
  }
}

function mapStateToProps(state) {
  // Whatever is returned will show up as props
  // inside of PetList
  return {
    Pets: state.Pets
  };
}

// Anything returned from this function will end up as props
// on the PetList container
function mapDispatchToProps(dispatch) {
  // Whenever selectPet is called, the result shoudl be passed
  // to all of our reducers
  return bindActionCreators({ choosePet: choosePet }, dispatch);
}

// Promote PetList from a component to a container - it needs to know
// about this new dispatch method, selectPet. Make it available
// as a prop.
export default connect(mapStateToProps, mapDispatchToProps)(PetList);
