import React, { Component } from "react";
import { connect } from "react-redux";

class PetDetail extends Component {
  render() {
    if (!this.props.Pet) {
      return <div>Select a Pet to get started.</div>;
    }

    return (
      <div>
        <h3>Details for:</h3>
        <div>Name: {this.props.Pet.name}</div>
        <div>Type: {this.props.Pet.type}</div>
        <div>Age: {this.props.Pet.age}</div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    Pet: state.selectedPet
  };
}

export default connect(mapStateToProps)(PetDetail);
