import React from "react";
import { Component } from "react";

import PetList from "../containers/pet-list";
import PetDetail from "../containers/pet-detail";

export default class App extends Component {
  render() {
    return (
      <div>
        <PetList />
        <PetDetail />
      </div>
    );
  }
}
