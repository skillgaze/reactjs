import { combineReducers } from "redux";
import PetsReducer from "./reducer_pets";
import selectedPet from "./reducer_selected_pet";

const rootReducer = combineReducers({
  Pets: PetsReducer,
  selectedPet: selectedPet
});

export default rootReducer;
