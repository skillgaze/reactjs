export default function() {
  return [
   {name:"Buddy", type: "pug", age:2}, 
   {name: "Angel", type: "poodle", age:3}, 
   {name: "Nemo", type: "Clown Fish", age:1}
  ];
}
