// this reducer is responsible for passing the selected pet from action to store
export default function(state = null, action) {
  switch (action.type) {
    case "PET_SELECTED":
      return action.payload;
  }

  return state;
}
